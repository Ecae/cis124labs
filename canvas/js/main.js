var canvas = document.getElementById('drawIt');
var context = canvas.getContext('2d');

var gradient = context.createLinearGradient(0, 0, canvas.width, 0);

/*
var background = new Image();
background.src = "https://lh3.googleusercontent.com/-R5HPaVYFkA4/AAAAAAAAAAI/AAAAAAAAANs/qKo7SSFRTBg/photo.jpg";

background.onload = function () {
    context.drawImage(background, 0, 0);
}


context.beginPath();
context.rect(0, 0, canvas.width, canvas.height);
context.fillStyle = "chartreuse";
context.fill();
*/


gradient.addColorStop(0, "black");
gradient.addColorStop(0.5, "white");
gradient.addColorStop(1, "red");


context.font = "50px Comic Sans MS";

context.strokeStyle = gradient;

context.strokeText('Memes', 5, 50, 200);
context.strokeText('Memes', 5, 100, 200);
context.strokeText('Memes', 5, 150, 200);
context.strokeText('Memes', 5, 200, 200);
context.strokeText('Memes', 5, 250, 200);
context.strokeText('Memes', 5, 300, 200);
context.strokeText('Memes', 5, 350, 200);
context.strokeText('Memes', 5, 400, 200);
context.strokeText('Memes', 5, 450, 200);
context.strokeText('Memes', 5, 500, 200);

context.strokeText('Memes', 25, 50, 200);
context.strokeText('Memes', 25, 100, 200);
context.strokeText('Memes', 25, 150, 200);
context.strokeText('Memes', 25, 200, 200);
context.strokeText('Memes', 25, 250, 200);
context.strokeText('Memes', 25, 300, 200);
context.strokeText('Memes', 25, 350, 200);
context.strokeText('Memes', 25, 400, 200);
context.strokeText('Memes', 25, 450, 200);
context.strokeText('Memes', 25, 500, 200);

context.strokeText('Memes', 25, 75, 200);
context.strokeText('Memes', 25, 125, 200);
context.strokeText('Memes', 25, 175, 200);
context.strokeText('Memes', 25, 225, 200);
context.strokeText('Memes', 25, 275, 200);
context.strokeText('Memes', 25, 325, 200);
context.strokeText('Memes', 25, 375, 200);
context.strokeText('Memes', 25, 425, 200);
context.strokeText('Memes', 25, 475, 200);

context.strokeText('Memes', 200, 75, 200);
context.strokeText('Memes', 200, 125, 200);
context.strokeText('Memes', 200, 175, 200);
context.strokeText('Memes', 200, 225, 200);
context.strokeText('Memes', 200, 275, 200);
context.strokeText('Memes', 200, 325, 200);
context.strokeText('Memes', 200, 375, 200);
context.strokeText('Memes', 200, 425, 200);
context.strokeText('Memes', 200, 475, 200);

context.strokeText('Memes', 375, 75, 200);
context.strokeText('Memes', 375, 125, 200);
context.strokeText('Memes', 375, 175, 200);
context.strokeText('Memes', 375, 225, 200);
context.strokeText('Memes', 375, 275, 200);
context.strokeText('Memes', 375, 325, 200);
context.strokeText('Memes', 375, 375, 200);
context.strokeText('Memes', 375, 425, 200);
context.strokeText('Memes', 375, 475, 200);

context.strokeText('Memes', 550, 75, 200);
context.strokeText('Memes', 550, 125, 200);
context.strokeText('Memes', 550, 175, 200);
context.strokeText('Memes', 550, 225, 200);
context.strokeText('Memes', 550, 275, 200);
context.strokeText('Memes', 550, 325, 200);
context.strokeText('Memes', 550, 375, 200);
context.strokeText('Memes', 550, 425, 200);
context.strokeText('Memes', 550, 475, 200);


context.strokeText('Memes', 50, 50, 200);
context.strokeText('Memes', 50, 100, 200);
context.strokeText('Memes', 50, 150, 200);
context.strokeText('Memes', 50, 200, 200);
context.strokeText('Memes', 50, 250, 200);
context.strokeText('Memes', 50, 300, 200);
context.strokeText('Memes', 50, 350, 200);
context.strokeText('Memes', 50, 400, 200);
context.strokeText('Memes', 50, 450, 200);
context.strokeText('Memes', 50, 500, 200);

context.strokeText('Memes', 100, 50, 200);
context.strokeText('Memes', 100, 100, 200);
context.strokeText('Memes', 100, 150, 200);
context.strokeText('Memes', 100, 200, 200);
context.strokeText('Memes', 100, 250, 200);
context.strokeText('Memes', 100, 300, 200);
context.strokeText('Memes', 100, 350, 200);
context.strokeText('Memes', 100, 400, 200);
context.strokeText('Memes', 100, 450, 200);
context.strokeText('Memes', 100, 500, 200);

context.strokeText('Memes', 150, 50, 200);
context.strokeText('Memes', 150, 100, 200);
context.strokeText('Memes', 150, 150, 200);
context.strokeText('Memes', 150, 200, 200);
context.strokeText('Memes', 150, 250, 200);
context.strokeText('Memes', 150, 300, 200);
context.strokeText('Memes', 150, 350, 200);
context.strokeText('Memes', 150, 400, 200);
context.strokeText('Memes', 150, 450, 200);
context.strokeText('Memes', 150, 500, 200);

context.strokeText('Memes', 200, 50, 200);
context.strokeText('Memes', 200, 100, 200);
context.strokeText('Memes', 200, 150, 200);
context.strokeText('Memes', 200, 200, 200);
context.strokeText('Memes', 200, 250, 200);
context.strokeText('Memes', 200, 300, 200);
context.strokeText('Memes', 200, 350, 200);
context.strokeText('Memes', 200, 400, 200);
context.strokeText('Memes', 200, 450, 200);
context.strokeText('Memes', 200, 500, 200);

context.strokeText('Memes', 250, 50, 200);
context.strokeText('Memes', 250, 100, 200);
context.strokeText('Memes', 250, 150, 200);
context.strokeText('Memes', 250, 200, 200);
context.strokeText('Memes', 250, 250, 200);
context.strokeText('Memes', 250, 300, 200);
context.strokeText('Memes', 250, 350, 200);
context.strokeText('Memes', 250, 400, 200);
context.strokeText('Memes', 250, 450, 200);
context.strokeText('Memes', 250, 500, 200);

context.strokeText('Memes', 300, 50, 200);
context.strokeText('Memes', 300, 100, 200);
context.strokeText('Memes', 300, 150, 200);
context.strokeText('Memes', 300, 200, 200);
context.strokeText('Memes', 300, 250, 200);
context.strokeText('Memes', 300, 300, 200);
context.strokeText('Memes', 300, 350, 200);
context.strokeText('Memes', 300, 400, 200);
context.strokeText('Memes', 300, 450, 200);
context.strokeText('Memes', 300, 500, 200);

context.strokeText('Memes', 350, 50, 200);
context.strokeText('Memes', 350, 100, 200);
context.strokeText('Memes', 350, 150, 200);
context.strokeText('Memes', 350, 200, 200);
context.strokeText('Memes', 350, 250, 200);
context.strokeText('Memes', 350, 300, 200);
context.strokeText('Memes', 350, 350, 200);
context.strokeText('Memes', 350, 400, 200);
context.strokeText('Memes', 350, 450, 200);
context.strokeText('Memes', 350, 500, 200);

context.strokeText('Memes', 400, 50, 200);
context.strokeText('Memes', 400, 100, 200);
context.strokeText('Memes', 400, 150, 200);
context.strokeText('Memes', 400, 200, 200);
context.strokeText('Memes', 400, 250, 200);
context.strokeText('Memes', 400, 300, 200);
context.strokeText('Memes', 400, 350, 200);
context.strokeText('Memes', 400, 400, 200);
context.strokeText('Memes', 400, 450, 200);
context.strokeText('Memes', 400, 500, 200);

context.strokeText('Memes', 450, 50, 200);
context.strokeText('Memes', 450, 100, 200);
context.strokeText('Memes', 450, 150, 200);
context.strokeText('Memes', 450, 200, 200);
context.strokeText('Memes', 450, 250, 200);
context.strokeText('Memes', 450, 300, 200);
context.strokeText('Memes', 450, 350, 200);
context.strokeText('Memes', 450, 400, 200);
context.strokeText('Memes', 450, 450, 200);
context.strokeText('Memes', 450, 500, 200);

context.strokeText('Memes', 500, 50, 200);
context.strokeText('Memes', 500, 100, 200);
context.strokeText('Memes', 500, 150, 200);
context.strokeText('Memes', 500, 200, 200);
context.strokeText('Memes', 500, 250, 200);
context.strokeText('Memes', 500, 300, 200);
context.strokeText('Memes', 500, 350, 200);
context.strokeText('Memes', 500, 400, 200);
context.strokeText('Memes', 500, 450, 200);
context.strokeText('Memes', 500, 500, 200);

context.strokeText('Memes', 550, 50, 200);
context.strokeText('Memes', 550, 100, 200);
context.strokeText('Memes', 550, 150, 200);
context.strokeText('Memes', 550, 200, 200);
context.strokeText('Memes', 550, 250, 200);
context.strokeText('Memes', 550, 300, 200);
context.strokeText('Memes', 550, 350, 200);
context.strokeText('Memes', 550, 400, 200);
context.strokeText('Memes', 550, 450, 200);
context.strokeText('Memes', 550, 500, 200);


/*
context.beginPath();
context.arc(500, 50, 40, 0, 2 * Math.PI);
context.stroke();
*/