//jQuery Button Demo



$(document).ready(function () {

    var monkeyDiv = $("#wiseMonkey");
    var monkeyPic;
    
    var message;
    
    var monkeyDiv2 = $("#wiseMonkey2");
    var monkeyPic2;
    
    var i = 0;
    
    $('button').each(function () {
        $(this).click(function () {
            if(i === 0){
                monkeyPic = $(this).find("img").attr("src");
                message = $(this).find("img").attr("alt");
                monkeyDiv.css("background-image","url(" + monkeyPic + ")");
                i++;
            }
            else if(i === 1){
                monkeyPic2 = $(this).find("img").attr("src");
                message = $(this).find("img").attr("alt");
                monkeyDiv2.css("background-image","url(" + monkeyPic2 + ")");
                i++;
            }
            
            else{
                Matching(monkeyDiv, monkeyDiv2);
                monkeyDiv.css("background-image","url(../media/img/empty.png)");
                monkeyDiv2.css("background-image","url(../media/img/empty.png)");
                i = 0;

            }
        });
        
        function Matching(pic,pic2) {
            if(pic.css("background-image") === pic2.css("background-image")) {
                $("#message").html(message);
            }
            else{
                $("#message").html("That was unwise");
            }
        }
    });    
});