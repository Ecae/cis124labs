//Global variables
//create variables for all DOM objects that will be part of the
//application interactions
var code, random, msg;
$(function() {
    reset8Ball();
    $('form').submit(function(e) {
        e.preventDefault();
        if ($('#question').val() != ""){
            getFortune();
        }
        else {
            showAnswer('I cant answer that');
        }
        /*
            Ideally, you would want to get a fortune only if a
            question was actually provided in the text input.
            If the question input is empty, have an appropriate
            message sent to showAnswer to reflect that.
        */
    });
    
    $('#magic8ball').click(function(){
        reset8Ball();
    });
});

//Helper functions
var getRandomIndex = function(max) {
    code = Math.random();
    /*
        Since $('#question').val() produces a string
        this will always result in a value of zero
        once it is processed in the PHP script.
        
        Make sure that "code" ends up with a random
        integer value instead. You can use a simple
        application of the Math.random() function to
        do this, but make sure you end up with some
        integer value being stored in "code"
    */
};


var showAnswer = function(msg) {
    //This should be called in getFortune instead.
    //The showAnswer function should focus only on showing a message in
    //the answer element to best separate responsibilities in the program.
    $('#magic8ball').effect("shake", {times:2,direction:"up"}, 100);
    $('#answer').html(msg).delay('700').fadeIn('fast');
    $('img').delay('700').fadeOut('fast');
};


var getFortune = function() {
    getRandomIndex();
    //call getRandomIndex here instead of in showAnswer
    $.ajax({
        url:'php/fortune.php',
        
        type:'post',
        
        data: {"code":code},
        
        success: function(response) {
            showAnswer(response);
        },
        
        error: function(xhr) {
            showAnswer('u gotta write something idiot');
        },
    });
};

var reset8Ball = function() {
    $('#magic8ball').effect("shake", {times:1,direction:"up"}, 100);
    $('#answer').html('').fadeOut('fast'); 
    $('img').fadeIn('fast');
};

